package com.example.triviaapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class activity_login extends AppCompatActivity implements View.OnClickListener {
    //login page


    private EditText userName, password;//declaring the text boxes that take the users info
    SharedPreferences sp;//an object that helps with saving information
    Context context;//connection to the activity (xml page)
    int countUsers;//number of users in the app

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //these lines hide the action bar and create a full screen, the action bar will reappear on homepage
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        context = this;//saving current activity

        //connecting the edit text to xml
        userName = findViewById(R.id.username);
        password = findViewById(R.id.password);

        //saving the information from the xml in a "folder" and keeping it private
        sp = getSharedPreferences("details1", Context.MODE_PRIVATE);
    }


    @Override
    //activated when clicking login button - saves user info in the apps database and enters app
    public void onClick(View view) {
        //variable that can work with the information on the database
        userDataBase myUser = new userDataBase(activity_login.this);
        //using a function of the class that checks if this user exists (if it is returns 1)
        countUsers = myUser.checkForUser(userName.getText().toString(), password.getText().toString());

        if (countUsers == 1) //the user exists
        {
            //goes to homepage - enters app
            Intent intent = new Intent(this, MainActivity.class);
            SharedPreferences.Editor editor = sp.edit();//giving access to edit
            editor.putString("fname", userName.getText().toString());//writing to shared preference - the user name. so it will be used simply in the future
            editor.commit();//saving what we just did (username into sp)
            startActivity(intent);//after everything is saved we go to homepage
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);//another use of animation that makes a smoother transition for a better user experience(blink)
        }
        else
        {
            //the user is not in the apps database - doesn't let you in and toasting a massage
            Toast.makeText(context, "No such user!", Toast.LENGTH_SHORT).show();
        }
    }

    //an option to go straight to signup page
    public void tosign(View view) {
        //when clicking the button goes to signup page
        Intent intent = new Intent(this, signUpActivity.class);
        startActivity(intent);

        //another use of animation that makes a smoother transition for a better user experience(blink)
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }
}