package com.example.triviaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

public class logorsign extends AppCompatActivity {
    //this page is for choosing login or signup, also there is a way to control the music here
    //declaring the 2 buttons
    Button login;
    Button signup;
    //an image that is also a button
    ImageButton sound;
    //used to control the music (on and off)
    Intent musicIntnet;

    //the music logo that stops and starts the music
    ImageButton mbtn;

    //saving the state of the music at all times
    SharedPreferences sp2;
    Boolean musicon;//gets shared preferences value

    AirplaneModeChangeReceiver airplaneModeChangeReceiver = new AirplaneModeChangeReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logorsign);

        //these lines hide the action bar and create a full screen, the action bar will reappear on homepage
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        sound = findViewById(R.id.mb1);//gets the pic at the beginning

        //getting the music info from the service
        musicIntnet = new Intent(this, MyService.class);

        // Retrieve the shared preferences instance for "details2" with private access mode
        sp2 = getApplicationContext().getSharedPreferences("details2", Context.MODE_PRIVATE);
        // Retrieve the boolean value associated with the key "music" from shared preferences
        musicon = sp2.getBoolean("music", true);// the code is checking whether the value associated with the key "music" is present in the SharedPreferences object. If it is present, it retrieves the boolean value and stores it in the musicon variable. If it is not present, it sets the value of musicon to true.
        // If music is enabled, start the music service and set the volume up icon
        if (musicon)
        {
            startService(musicIntnet);
            sound.setImageResource(R.drawable.ic_baseline_volume_up_24);
        }
        else
        {
            // If music is disabled, stop the music service and set the volume off icon
            stopService(musicIntnet);
            sound.setImageResource(R.drawable.ic_baseline_volume_off_24);
        }

        //to get on the safe side, makes window left to right
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        //connecting to the id in the xml in order to work with them
        login = findViewById(R.id.loginbtn);
        signup = findViewById(R.id.signupbtn);
        mbtn = (ImageButton) findViewById(R.id.mb1);

    }

    //this function is activated when pressing the login button
    public void loginintent(View view)
    {
        //going to login page
        Intent intent = new Intent(this, activity_login.class);
        startActivity(intent);
        //another use of animation that makes a smoother transition for a better user experience(blink)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    //this function is activated when pressing the signup button
    public void signupintent(View view)
    {
        //going to signup page
        Intent intent = new Intent(this, signUpActivity.class);
        startActivity(intent);
        //another use of animation that makes a smoother transition for a better user experience(blink)
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    //this function is responsible for the music (turning it on and off)
    public void startmuic(View view)
    {
        //according to the button's state, the music starts or stops(change in state) - the info is saved in shared preference.
        if (musicon == false)
        {
            startService(musicIntnet);
            musicon = true;
            mbtn.setImageResource(R.drawable.ic_baseline_volume_up_24);
        }
        else
        {
            stopService(musicIntnet);
            musicon = false;
            mbtn.setImageResource(R.drawable.ic_baseline_volume_off_24);
        }
        //Save the current music state to SharedPreferences
        SharedPreferences.Editor editor = sp2.edit();
        editor.putBoolean("music", musicon);
        editor.apply();
    }

    //control the broadcast receiver and know if airplane mode is on or off
    @Override
    protected void onStart()
    {
        super.onStart();
        IntentFilter filter = new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        registerReceiver(airplaneModeChangeReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(airplaneModeChangeReceiver);
    }
}