package com.example.triviaapp;

//questions, option answers and correct answers class (Entertainment)
//this class has no functions only variables
public class QEntertainmentSet {
    public static String question[] =
            {
                    "What is the most successful television show of all times?",
                    "What was the first feature-length animated movie ever released? (Hint: It was a Disney Movie)",
                    "What TV series showed the first interracial kiss on American network television?",
                    "What were the four main characters' names in the TV series “Golden Girls“ that ran from 1985-1992?",
                    "Which Disney Princess talks to the most animals?",
                    "What is the most viewed video on youtube",
                    "Which member of the Beatles married Yoko Ono?",
                    "What are the names of Cinderella’s stepsisters?",
                    "What famous US festival hosted over 350,000 fans in 1969?",
                    "The biggest selling music single of all time is?"



            };
    public static String choices[][] =
            {
                    {"The Simpsons", "Friends", "How I met your mother", "Family guy"},
                    {"Bambi", "Cinderella", "Snow White and the Seven Dwarfs", "Micky Mouse"},
                    {"Star Wars", "The Brady Bunch", "mad men", "Star Trek"},
                    {"Dorothy, Rose, Amy and Lisa", "Dorothy, Rose, Blanche, and Sophia", "Blanche, Lili , Maddie and Sophia", "Mia, Emily, Michell and Rosa"},
                    {"Bell", "The Sleeping Beauty", "Snow White", "Ariel"},
                    {"baby shark", "Despacito", "Gangnam style", "Shape of You"},
                    {"Paul McCartney", "John Lennon", "Ringo star", "George Harrison"},
                    {"Anastasia and Drizella", "Olivia and charlotte", "Sophia and Isabella", "ava and Michell"},
                    {"BottleRock Music Festival", "Coachella", "Burning Man", "Woodstock"},
                    {"I Will Always Love You", "Candle in the Wind", "My Heart Will Go On", "Rock Around the Clock"}


            };
    public static String correctAnswers[] =
            {
                    "The Simpsons",
                    "Snow White and the Seven Dwarfs",
                    "Star Trek",
                    "Dorothy, Rose, Blanche, and Sophia",
                    "Snow White",
                    "baby shark",
                    "John Lennon",
                    "Anastasia and Drizella",
                    "Woodstock",
                    "Candle in the Wind"

            };
}
