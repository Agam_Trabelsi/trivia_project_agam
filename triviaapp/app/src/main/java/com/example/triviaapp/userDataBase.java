package com.example.triviaapp;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class userDataBase extends SQLiteOpenHelper {
    //constant values - define
    private static final String DATABASE_NAME = "APP37.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE01_NAME = "users";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";

    private static final String COLUMN_PASS = "password";
    private static final String COLUMN_SCORE = "score_value";
    private static final String COLUMN_IMAGE = "image";
    private static final String COLUMN_NUMBER = "number";



    private static final String TABLE02_NAME = "ScoreTable";
    private static final String COLUMN_USER_ID = "_id";
    private static final String COLUMN_USER_NAME = "user";
    private static final String COLUMN_USER_SCORE = "score";
    private static final String COLUMN_USER_IMAGE = "userimage";
    private static final String COLUMN_USER_NUMBER = "usernumber";


    private static final String[] allColumns = {COLUMN_IMAGE};

    private SQLiteDatabase database;
    private final Context context;

    //this function is a constructor, and called every time we want to refer the database
    public userDataBase(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    //creation of the 2 databases, one for leaderboard and one for user
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        String queryUserTable = "CREATE TABLE IF NOT EXISTS " + TABLE01_NAME +
                " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME + " TEXT, " +
                COLUMN_NUMBER + " INTEGER, " +
                COLUMN_IMAGE + " BLOB, " +
                COLUMN_SCORE + " TEXT, " +
                COLUMN_PASS + " TEXT);";

        String queryScoreTable = "CREATE TABLE IF NOT EXISTS " + TABLE02_NAME +
                " (" + COLUMN_ID + " INTEGER PRIMARY KEY, " +
                COLUMN_USER_ID + " INTEGER, " +
                COLUMN_USER_NUMBER + " INTEGER, " +
                COLUMN_USER_NAME + " TEXT, " +
                COLUMN_USER_IMAGE + " BLOB, " +
                COLUMN_USER_SCORE + " TEXT);";

        //creates database
        sqLiteDatabase.execSQL(queryUserTable);
        sqLiteDatabase.execSQL(queryScoreTable);
    }

    //everytime we install the app we need the databse to work with the right version, a way of doing it is recreation of the database
    //איתחול בעת הורדה מחדש
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1)
    {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE01_NAME);
        onCreate(sqLiteDatabase);
        if (i > i1)
        {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE01_NAME);
            onCreate(sqLiteDatabase);
        }
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE02_NAME);
    }

    //puts a new user inside the users database
    public void insertUser(String name, String password)
    {
        SQLiteDatabase db = this.getWritableDatabase();//gives access to write on the database adding users in that case
        ContentValues cv = new ContentValues();//class that we use for the function put in order to insert data

        cv.put(COLUMN_NAME, name);
        cv.put(COLUMN_PASS, password);
        long result = db.insert(TABLE01_NAME, null, cv);
        if (result == -1)
        {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
        } else
        {
            Toast.makeText(context, "Added Successfully!", Toast.LENGTH_SHORT).show();//toast we see on signup
        }

    }

    //checks if the user is in the database(for login)
    public int checkForUser(String name, String userPassword)
    {
        SQLiteDatabase db = this.getReadableDatabase();//opens database for read only
        String query = "SELECT id FROM " + TABLE01_NAME + " WHERE name = '" + name + "' AND password = '" + userPassword + "'";//go to use database and find name and password that the user put in the login

        Cursor cursor = null;
        if (db != null)
        {
            cursor = db.rawQuery(query, null);//activates query
        }

        return cursor.getCount();//there should be only one user
    }

    //checks if the user name exists in the database(for signup toasts)
    public int doesExist(String name)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT id FROM " + TABLE01_NAME + " WHERE name = '" + name + "'";

        Cursor cursor = null;
        if (db != null)
        {
            cursor = db.rawQuery(query, null);
        }

        return cursor.getCount();
    }
    //checks if the password exists in the database(for signup toasts)
    public int doesPasswordExist(String userPassword)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT id FROM " + TABLE01_NAME + " WHERE password = '" + userPassword + "'";

        Cursor cursor = null;
        if (db != null) {
            cursor = db.rawQuery(query, null);
        }

        return cursor.getCount();
    }


    //enter pic to user profile
    public long addPictoProf(Pic userProfilepic, String name)
    {
        long id = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String selection = "id = ?";
        Cursor cursor = null;
        String query = "SELECT id FROM " + TABLE01_NAME + " WHERE name = '" + name + "'";
        if (db != null)
        {
            cursor = db.rawQuery(query, null);
        }
        cursor.moveToFirst();//moves to the first line that is valid to the query
        @SuppressLint("Range") int a = cursor.getInt(cursor.getColumnIndex("id"));//gets id result

        contentValues.put(COLUMN_IMAGE, Helper.bitmapToByteArray(userProfilepic.getBitmap()));//puts the photo in the database as bytearray in the right id
        db.update(TABLE01_NAME, contentValues, selection, new String[]{String.valueOf(a)});  //update database with new information

        return id;
    }

    //extract picture from data base
    @SuppressLint("Range")
    public Bitmap getPic(long id)
    {

        byte[] rv = new byte[0];
        SQLiteDatabase db = this.getWritableDatabase();
        String whereclause = "id =?";
        String[] whereargs = new String[]{String.valueOf(id)};
        Cursor csr = db.query(TABLE01_NAME, null, whereclause, whereargs, null, null, null);//sets cursor on the right line in order to extract pic
        if (csr.moveToFirst())//if not null and database has info
        {
            rv = csr.getBlob(csr.getColumnIndex(COLUMN_IMAGE));
            return Helper.byteArrayToBitmap(rv);
        }
        else
            return null;
    }


    //updates the total score in the USER database
    public void addScorenew(String score, String name)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        // check if the user already exists in the table
        String query = "SELECT id, score_value FROM " + TABLE01_NAME + " WHERE name = '" + name + "'";//gets the id and total score of the requested name
        Cursor cursor = db.rawQuery(query, null);//sets the cursor on that position

        if (cursor.moveToFirst())//data base is not empty and moves the cursor to first
        {
            // user already exists, check if the new score is higher
            @SuppressLint("Range") int id = cursor.getInt(cursor.getColumnIndex("id"));//goes to the right id
            @SuppressLint("Range") int existingScore = cursor.getInt(cursor.getColumnIndex("score_value"));//gets current score
            int newScore = Integer.parseInt(score);//the points we just won
            if (newScore > existingScore)//i answered at least on 1 question
            {
                // update the existing record with the new score
                ContentValues values = new ContentValues();
                values.put(COLUMN_SCORE, newScore);
                db.update(TABLE01_NAME, values, "id = ?", new String[]{String.valueOf(id)});
            }
        }
        else
        {
            // user doesn't exist, insert a new record
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME, name);
            values.put(COLUMN_SCORE, score);
            db.insert(TABLE01_NAME, null, values);
        }
        cursor.close();
        db.close();
    }


    //using the id returns the score of this id
    @SuppressLint("Range")
    public String getAllScores(long id)
    {
        String rv = "not found";
        SQLiteDatabase db = this.getWritableDatabase();
        String whereclause = "id =?";
        String[] whereargs = new String[]{String.valueOf(id)};
        Cursor csr = db.query(TABLE01_NAME, null, whereclause, whereargs, null, null, null);
        if (csr.moveToFirst())
        {
            rv = csr.getString(csr.getColumnIndex(COLUMN_SCORE));
        }
        return rv;
    }

    //using a name to find its id
    @SuppressLint("Range")
    public String getIdbyName(String name)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT id FROM " + TABLE01_NAME + " WHERE name = '" + name + "'";

        Cursor cursor = null;
        if (db != null)
        {
            cursor = db.rawQuery(query, null);
        }

        cursor.moveToFirst();
        int a = cursor.getInt(cursor.getColumnIndex("id"));

        return String.valueOf(a);
    }


    //counts the number of rows in the data base - meaning, number of users in the app
    public int getProfilesCount()
    {
        String countQuery = "SELECT  * FROM " + TABLE02_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    //updates the total score in the SCORE database (ׂfor documentation go back in th e page)
    public void addScoreusernew(String score, String name)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        // check if the user already exists in the table
        String query = "SELECT id, score FROM " + TABLE02_NAME + " WHERE user = '" + name + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst())
        {
            // user already exists, check if the new score is higher
            @SuppressLint("Range") int id = cursor.getInt(cursor.getColumnIndex("id"));
            @SuppressLint("Range") int existingScore = cursor.getInt(cursor.getColumnIndex("score"));
            int newScore = Integer.parseInt(score);
            if (newScore > existingScore)
            {
                // update the existing record with the new score
                ContentValues values = new ContentValues();
                values.put(COLUMN_USER_SCORE, newScore);
                db.update(TABLE02_NAME, values, "id = ?", new String[]{String.valueOf(id)});
            }
        }
        else
        {
            // user doesn't exist, insert a new record
            ContentValues values = new ContentValues();
            values.put(COLUMN_USER_NAME, name);
            values.put(COLUMN_USER_SCORE, score);
            db.insert(TABLE02_NAME, null, values);
        }

        cursor.close();
        db.close();
    }

    //enter pic to leader board
    public long addPictoProfuser(Pic userProfilepic, String name) {
        long id = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String selection = "user = ?";
        Cursor cursor = null;
        String query = "SELECT user FROM " + TABLE02_NAME + " WHERE user = '" + name + "'";
        if (db != null)
        {
            cursor = db.rawQuery(query, null);
        }
        cursor.moveToFirst();
        @SuppressLint("Range") String a = cursor.getString(cursor.getColumnIndex("user"));

        contentValues.put(COLUMN_USER_IMAGE, Helper.bitmapToByteArray(userProfilepic.getBitmap()));
        db.update(TABLE02_NAME, contentValues, selection, new String[]{a});  // number 1 is the _id here, update to variable for your code

        return id;
    }

    //gets score by id
    @SuppressLint("Range")
    public String getAllScoresuser(long id)
    {
        String rv = "not found";
        SQLiteDatabase db = this.getWritableDatabase();
        String whereclause = "id =?";
        String[] whereargs = new String[]{String.valueOf(id)};
        Cursor csr = db.query(TABLE02_NAME, null, whereclause, whereargs, null, null, null);//gets the row of the wanted id
        if (csr.moveToFirst())//database not empty
        {
            rv = csr.getString(csr.getColumnIndex(COLUMN_USER_SCORE));
        }
        return rv;
    }

    //delete a user according to name
    public void deleteUser(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE01_NAME + " WHERE " + COLUMN_NAME + "='" + name + "'");
        db.execSQL("DELETE FROM " + TABLE02_NAME + " WHERE " + COLUMN_USER_NAME + "='" + name + "'");
        db.close();
    }


    //Insert number to table 1 if doesn't exit but if exist its will update his number(Each number = specific avatar)
    public void updateUserNumber(String name, int number)//name and a number that represents an avatar
    {
        SQLiteDatabase db = this.getWritableDatabase();

        // Check if the user already exists in the table
        String selectQuery = "SELECT * FROM " + TABLE01_NAME + " WHERE " + COLUMN_NAME + "=?";
        Cursor cursor = db.rawQuery(selectQuery, new String[]{name});

        ContentValues values = new ContentValues();
        values.put(COLUMN_NUMBER, number);

        if (cursor.getCount() > 0)//name found in db
        {
            // User already exists, update their number
            db.update(TABLE01_NAME, values, COLUMN_NAME + "=?", new String[]{name});
        }
        else
        {
            // User does not exist, insert a new row
            values.put(COLUMN_NAME, name);
            db.insert(TABLE01_NAME, null, values);
        }

        cursor.close();
        db.close();
    }

    //Insert number to table 2 if doesn't exit but if exist its will update his number(Each number = specific avatar)
    public void updateUsertableNumber(String name, int number) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Check if the user already exists in the table
        String selectQuery = "SELECT * FROM " + TABLE02_NAME + " WHERE " + COLUMN_USER_NAME + "=?";
        Cursor cursor = db.rawQuery(selectQuery, new String[]{name});

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NUMBER, number);

        if (cursor.getCount() > 0)
        {
            // User already exists, update their number
            db.update(TABLE02_NAME, values, COLUMN_USER_NAME + "=?", new String[]{name});
        }
        else
        {
            // User does not exist, insert a new row
            values.put(COLUMN_USER_NAME, name);
            db.insert(TABLE02_NAME, null, values);
        }

        cursor.close();
        db.close();
    }

    //get the value of the number from table 2 meant for the leaderboard.
    @SuppressLint("Range")
    public int getUserNumber(String name)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        // Select the user's number from the table
        String selectQuery = "SELECT " + COLUMN_NUMBER + " FROM " + TABLE01_NAME + " WHERE " + COLUMN_NAME + "=?";
        Cursor cursor = db.rawQuery(selectQuery, new String[]{name});//sets the cursor on rows that stand in all criteria

        int number = 0;

        if (cursor.moveToFirst())
        {
            // Retrieve the user's number from the cursor
            number = cursor.getInt(cursor.getColumnIndex(COLUMN_NUMBER));
        }

        cursor.close();
        db.close();

        return number;
    }
}
