package com.example.triviaapp;

//questions, option answers and correct answers class (geography)
//this class has no functions only variables
public class QGeographySet
{
    public static String question[] =
            {
                    "What is Earth's largest continent?",
                    "What's the smallest country in the world?",
                    "Area 51 is located in which U.S state?",
                    "What country touches the Indian Ocean, the Arabian Sea, and the Bay of Bengal?",
                    "What's the city with the most diversity in terms of language?",
                    "The ancient Phoenician city of Constantine is located in what modern-day Arab country?",
                    "Which country borders 14 nations and crosses 8 time zones?",
                    "Havana is the capital of what country?",
                    "What country has the most natural lakes?",
                    "Riyadh is the capital of this Middle-Eastern country."


            };
    public static String choices[][] =
            {
                    {"Asia", "Australia", "Africa", "Europe"},
                    {"Monaco", "Vatican City", "Liechtenstein", "Tuvalu"},
                    {"Alabama", "Nevada", "Texas", "Arizona"},
                    {"The Philippines", "Turkey", "Canada", "India"},
                    {"Singapore", "Jerusalem", "New York City", "Macau"},
                    {"Qatar", "Algeria", "Bahrain", "Iraq"},
                    {"Russia", "China", "Brazil", "Switzerland"},
                    {"Mexico", "Chile", "Cuba", "Colombia"},
                    {"Canada", "Sweden", "Australia", "Spain"},
                    {"Iran", "Egypt", "Qatar", "Saudi Arabia"}



            };
    public static String correctAnswers[] =
            {
                    "Asia",
                    "Vatican City",
                    "Nevada",
                    "India",
                    "New York City",
                    "Algeria",
                    "Russia",
                    "Cuba",
                    "Canada",
                    "Saudi Arabia"

            };
}
