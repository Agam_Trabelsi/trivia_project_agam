package com.example.triviaapp;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.ModuleInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import java.util.Random;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class splash extends AppCompatActivity {
    //declaration of variables
    ImageView imageView, imageView2;//declaring the 2 sliding pictures - app name (trivia master)
    Animation animSlide, animSlideleft;//declaring the animation - which allows the movement in that page. (animSlide - slides right)
    Intent musicIntnet;//responsible on the music playing in the background (starting or stopping music)
    TextView slog;//random sentence in the end of the page
    String[] slogArr = { "Questions You Will Never Forget!",
            "A Game That Makes Learning Fun",
            "The best Questions You Have Ever Seen!",
            "True Brain Crackers",
            "It's Time To Gain Some Knowledge!"};//array of all slogans

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);//connected to splash activity

        //creating java variables using their id in the xml page (the 2 pictures - app name)
        imageView = (ImageView) findViewById(R.id.img);
        imageView2 = (ImageView) findViewById(R.id.img2);

        //random slogan in the beginning
        slog = findViewById(R.id.slogan);
        Random rndSentence = new Random();
        slog.setText(slogArr[rndSentence.nextInt(5)]);

        //loading the animation - one slides to the right and the other to the left (the specific definitions are in anim folder)
        animSlide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide);
        animSlideleft = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slideleft);

        //the next section is created to keep the animation in place
        //defining a listener that tracks the animation
        animSlide.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {
                // Animation started
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // Animation ended
                animation.setFillAfter(true);//only the end of the animation matters, and that is the commend that says - hold.
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // Animation repeated
            }
            //start and repeat are not relevant in this case  - that is why they are empty(and yet, can't be deleted, because the listener has to get something)
        });

        //same thing here for the other picture
        animSlideleft.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
                // Animation started
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                // Animation ended
                animation.setFillAfter(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
                // Animation repeated
            }
        });

        //activates the animation - only here it actually starts.
        imageView.startAnimation(animSlide);
        imageView2.startAnimation(animSlideleft);

        //using the music service class in order to start playing the music
        musicIntnet = new Intent(this, MyService.class);
        startService(musicIntnet);

        //these lines hide the action bar and create a full screen, the action bar will reappear on homepage
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        //this function makes sue the transition will occur only once the animation ends, only then the intent kicks in
        new Handler().postDelayed(new Runnable() {
            @Override
            //this function closes this page and goes to the next one
            public void run() {
                //intent that leads to the login/signup page
                Intent intent = new Intent(splash.this, logorsign.class);
                startActivity(intent);
                finish();//closing the current page(splash)
                //another use of animation that makes a smoother transition for a better user experience(blink)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        }, 4000);//1000ms = 1sec - waits 4 seconds before going to the next page

    }
}