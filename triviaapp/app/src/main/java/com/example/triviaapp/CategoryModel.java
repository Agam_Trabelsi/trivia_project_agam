package com.example.triviaapp;
import java.io.Serializable;

//creating a class for a single category that contains an id and a picture of the category
public class CategoryModel implements Serializable
{
    //declaring variables - id image and name
    private String id;
    private int image;
    private String name;

    //constructor
    public CategoryModel(String id, String name,int image)
    {
        this.id = id;
        this.image = image;
        this.name = name;
    }

    //getters and setters
    public int getImage() {return image;}

    public void setImage(int image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
