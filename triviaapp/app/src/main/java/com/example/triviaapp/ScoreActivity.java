package com.example.triviaapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ScoreActivity extends AppCompatActivity {
    //this page appear after a trivia game and shows the score from the current game and the total score of the user

    private TextView tvscore, status;
    private TextView tvHighScore;
    private Button done;
    int score;
    String playerstatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        //creating a data base variable that takes info from it
        userDataBase db = new userDataBase(this);

        //connection to xml
        tvscore = findViewById(R.id.sa_score);
        tvHighScore = (TextView) findViewById(R.id.best_score);
        status = (TextView) findViewById(R.id.status);
        done = findViewById(R.id.sa_done);

        //sets page left to right
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        setTitle("Final Score");
        //getting information from the shared preference that contains user details - username
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("details1", Context.MODE_PRIVATE);
        //saving information as string
        String fname = sharedPreferences.getString("fname", "");

        //the question page has putextra in it and this gets that info - its the points from the current round
        score = getIntent().getIntExtra("point", 0);
        tvscore.setText(Integer.toString(score));//makes the text to be that score in the activity

        //putextra gives the status of the player
        //according to string - sets the text color
        playerstatus = getIntent().getStringExtra("status");
        if (playerstatus.equals("Good job!"))
        {
            status.setTextColor(Color.rgb(50, 205, 50));
        }
        else if(playerstatus.equals("Excellent work!"))
        {
            status.setTextColor(Color.rgb(192, 192, 192));
        }
        else
        {
            status.setTextColor(Color.rgb(255, 0, 0));
        }
        status.setText(playerstatus);//defines the texts accordingly


        String allScores = db.getAllScores(Long.parseLong(db.getIdbyName(fname)));//total score
        int newscore = score;
        if (allScores != null)
        {
            newscore += Integer.parseInt(allScores);//adds game score to total
        }

        tvHighScore.setText(String.valueOf(newscore));//shows total on the screen

        //sets the 2 databases with the new total
        db.addScorenew(String.valueOf(newscore), fname);
        db.addScoreusernew(String.valueOf(newscore), fname);


        //listener that when clicked goes back to categories page(done button)
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScoreActivity.this, CategoryActivity.class);
                ScoreActivity.this.startActivity(intent);
                ScoreActivity.this.finish();
                //nice transition
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });
    }
}
