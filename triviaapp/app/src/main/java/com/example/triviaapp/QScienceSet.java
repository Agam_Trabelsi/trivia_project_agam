package com.example.triviaapp;

//questions, option answers and correct answers class (science)
//this class has no functions only variables
public class QScienceSet
{

    public static String question[] =
            {
                    "In what type of matter are atoms most tightly packed?",
                    "What is the hottest planet in the solar system?",
                    "What is the opposite of matter?",
                    "Which of Newton’s Laws states that ‘for every action, there is an equal and opposite reaction?",
                    "In 2004, what was discovered on the island of Flores in Indonesia?",
                    "What is the nearest planet to the sun?",
                    "What color is your blood when it’s inside your body?",
                    "What is the largest planet in the solar system?",
                    "How many teeth does an adult human have?",
                    "What's the largest bone in the human body?"
            };
    public static String choices[][] =
            {
                    {"liquids", "jelly", "gases", "Solids"},
                    {"Venus", "Mercury", "Uranus", "Earth"},
                    {"Waves", "dark matter", "Antimatter", "molecules"},
                    {"The first law", "The third law", "the second law", "the fourth law"},
                    {"Remains of a hobbit-sized human", "a new butterfly species", "an alien colony", "archeology findings from the 5th century"},
                    {"Venus", "Mars", "Neptune", "Mercury"},
                    {"White", "Clear", "Pink", "Red"},
                    {"Uranus", "Jupiter", "Saturn", "Earth"},
                    {"32", "40", "22", "28"},
                    {"coccyx", "sternum", "Femur", "scapula"}

            };
    public static String correctAnswers[] =
            {
                    "Solids",
                    "Venus",
                    "Antimatter",
                    "The third law",
                    "Remains of a hobbit-sized human",
                    "Mercury",
                    "Red",
                    "Jupiter",
                    "32",
                    "Femur"
            };
}
