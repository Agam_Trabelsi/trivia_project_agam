package com.example.triviaapp;

//questions, option answers and correct answers class (food and drinks)
//this class has no functions only variables
public class QFooddrinksSet
{
    public static String question[] =
            {
                    "What is the rarest M&M color?",
                    "What is the common name for dried plums?",
                    "Which country consumes the most chocolate per capita?",
                    "What is the name given to Indian food cooked over charcoal in a clay oven?",
                    "What was the first soft drink in space?",
                    "What is the most consumed manufactured drink in the world?",
                    "Which is the only edible food that never goes bad?",
                    "Which country invented ice cream?",
                    "“Hendrick’s”, “Larios”, and “Seagram’s” are some of the best-selling brands of which spirit?",
                    "From which country does Gouda cheese originate?"



            };
    public static String choices[][] =
            {
                    {"Brown", "Blue", "Yellow", "Red"},
                    {"Khajoor", "Raisins", "Prunes", "Akhrot"},
                    {"Belgium", "Switzerland", "USA", "Spain"},
                    {"Tandoori", "Pakora", "Chaat", "Paratha"},
                    {"Orange Juice", "Fanta", "Pepsi", "Coca Cola"},
                    {"coffee", "Tea", "cola", "milk"},
                    {"Honey", "sugar", "salt", "almonds"},
                    {"Italy", "Germany", "Sweden", "China"},
                    {"Vodka", "Gin", "Rum", "Whisky"},
                    {"Ukraine", "UK", "Netherlands", "Switzerland"}



            };
    public static String correctAnswers[] =
            {
                    "Brown",
                    "Prunes",
                    "Switzerland",
                    "Tandoori",
                    "Coca Cola",
                    "Tea",
                    "Honey",
                    "China",
                    "Gin",
                    "Netherlands"


            };
}
