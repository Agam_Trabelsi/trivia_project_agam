package com.example.triviaapp;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.transition.Fade;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;


import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class CategoryActivity extends AppCompatActivity {
    //declaring variables for the categories
    private GridView catGrid;
    private String[] subject = {"Sports", "Science", "Geography", "Animals", "Entertainment", "Food & drinks"};
    private String[] id = {"Test_1", "Test_2", "Test_3", "Test_4", "Test_5", "Test_6"};
    private int[] images = {R.mipmap.sportico, R.mipmap.scienceico, R.mipmap.geographyico, R.mipmap.animalsico, R.mipmap.entertainmenticon, R.mipmap.foodico};
    private List<CategoryModel> subjectlist = new ArrayList<>();
    Intent musicIntnet;//in order to cut the music if pressed exit

    //Drawer's stuff
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ImageView userpic;
    userDataBase coh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        catGrid = findViewById(R.id.catGridview);
        setTitle("Categories");
        //the page should be left to right
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        //getting the music info from the service
        musicIntnet = new Intent(this, MyService.class);

        //loop through and add to list the different categories
        for (int i = 0; i < subject.length; i++)
        {
            CategoryModel itemsModal = new CategoryModel(id[i], subject[i], images[i]);
            subjectlist.add(itemsModal);
        }

        SubjectGrid adapter = new SubjectGrid(subjectlist, this);//creating adapter
        catGrid.setAdapter(adapter);


        //Make all the preparation for the drawer
        prepDrawer();

        //Calling to function that update the header of
        updateHeader();

        //Make the Drawer clickable like the menu
        clickDrawer();
    }


    public class SubjectGrid extends BaseAdapter {

        private List<CategoryModel> catList;
        private Context context;

        //constructor
        public SubjectGrid(List<CategoryModel> itemsModalList, Context context) {
            this.catList = itemsModalList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return catList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup parent) {
            View view1 = view;//creating a view variable

            CategoryModel itemsModal = catList.get(i);

            if (view1 == null)
            {
                //This code inflates a layout resource file called "cat_item_layout" and creates a standalone View object that can be accessed through the "view1" variable.
                view1 = LayoutInflater.from(context).inflate(R.layout.cat_item_layout, parent, false);//1 category design
            }

            //a single category info
            ImageView imageName = view1.findViewById(R.id.subjectimg);
            TextView tvName = view1.findViewById(R.id.catName);

            //name and picture for each category
            String name = itemsModal.getName();
            int image = itemsModal.getImage();

            //changes the picture and name for each category in the activity
            imageName.setImageResource(image);
            tvName.setText(name);
            ImageView img = view1.findViewById(R.id.subjectimg);//new var for animation

            // a random color to the cards background
            Random rnd = new Random();
            CardView card = view1.findViewById(R.id.cvitem);
            int color = Color.argb(255, rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255));
            card.setCardBackgroundColor(color);
            card.setElevation(30);

            view1.setOnClickListener(new View.OnClickListener() {//creating a listener on the screens view
                @Override
                public void onClick(View v) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(CategoryActivity.this, img, ViewCompat.getTransitionName(img));//this animation makes a nicer transition
                    startActivity(new Intent(CategoryActivity.this, QuestionActivity.class).putExtra("data", itemsModal).putExtra("color", color), options.toBundle());//goes to question activity and saves the picture and color of the category to present in the top left corner of the screen
                }
            });
            return view1;
        }
    }

    //creates menu
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    //menu function - goes to the right page accordingly
    public boolean onOptionsItemSelected(final MenuItem item) {
        //all the id's are from the menu xml
        int id = item.getItemId();
        //Now that our menu in the drawer we only need to show the burger icon and make in clickable with this small if
        if (id == android.R.id.home) {
            //If open show the open icon(arrow icon)
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                //Else show the close icon (burger icon)
                drawerLayout.openDrawer(GravityCompat.START);
            }
            return true;
        }
        return true;
    }

    public void alertDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);//creates a new dialog
        builder.setTitle("Exit app");//headline
        builder.setIcon(R.mipmap.exitapp);//icon on headline
        builder.setMessage("Are you sre you want to exit?");
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() //the answer yes
        {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();//closing dialog
                Toast.makeText(CategoryActivity.this, "Goodbye!", Toast.LENGTH_LONG).show();
                stopService(musicIntnet);
                finish();//closing the page
                finishAffinity();//closing the entire app
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() //the button will say no
        {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();//closes dialog only, not app
                Toast.makeText(CategoryActivity.this, "enjoy playing!", Toast.LENGTH_LONG).show();

            }
        });
        AlertDialog alertDialog = builder.create();//all we did was in theory, now the creation of the window happens
        alertDialog.show();//showing the dialog on screen
    }

    //a button that goes back to home page - activated when pressed
    public void backhome(View view) {
        Intent intent = new Intent(CategoryActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);//transition

    }

    //logout function and takes user back to before the login and clearing shared preference for the users to come
    public void logoutuser() {
        SharedPreferences preferences = getSharedPreferences("details1", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        SharedPreferences score = getSharedPreferences("Score", Context.MODE_PRIVATE);
        SharedPreferences.Editor edscore = score.edit();
        edscore.clear();
        edscore.commit();
        Intent intent = new Intent(this, logorsign.class);
        startActivity(intent);
    }

    //Functions to move between intents
    public void movetopageHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void movetopageBoard() {
        Intent intent = new Intent(this, LeaderBoard.class);
        startActivity(intent);
    }

    public void movetoprofile() {
        Intent intent = new Intent(this, userprofile.class);
        startActivity(intent);
    }

    //To make the code more clear(In some way) instead of put all of this in onCreate we make function that take care for this mess.
    public void prepDrawer() {
        coh = new userDataBase(this);
        //Declaring about Drawer verbs
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_baseline_menu_24);
        }
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.navview);
        //Creating the burger icon at the menu
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    //Update the header of the Drawer
    public void updateHeader() {
        Intent intent = getIntent();
        SharedPreferences sp = getApplicationContext().getSharedPreferences("details1", Context.MODE_PRIVATE);
        String fname = sp.getString("fname", "");
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = (TextView) headerView.findViewById(R.id.tvusername);
        navUsername.setText("Hello " + fname);
        userpic = (ImageView) headerView.findViewById(R.id.userpic);
        if ((coh.getPic(Long.parseLong(coh.getIdbyName(fname)))) != null)
        {
            if (coh.getUserNumber(fname) == 0) {
                userpic.setImageBitmap((coh.getPic(Long.parseLong(coh.getIdbyName(fname)))));
            } else if (coh.getUserNumber(fname) == 1) {
                userpic.setImageResource(R.mipmap.boy);
            } else if (coh.getUserNumber(fname) == 2) {
                userpic.setImageResource(R.mipmap.girl);
            } else if (coh.getUserNumber(fname) == 3) {
                userpic.setImageResource(R.mipmap.women);
            } else if (coh.getUserNumber(fname) == 4) {
                userpic.setImageResource(R.mipmap.man);
            } else if (coh.getUserNumber(fname) == 5) {
                userpic.setImageResource(R.mipmap.triviausericon);
            }
        }
        else
        {
            if (coh.getUserNumber(fname) == 1) {
                userpic.setImageResource(R.mipmap.boy);
            } else if (coh.getUserNumber(fname) == 2) {
                userpic.setImageResource(R.mipmap.girl);
            } else if (coh.getUserNumber(fname) == 3) {
                userpic.setImageResource(R.mipmap.women);
            } else if (coh.getUserNumber(fname) == 4) {
                userpic.setImageResource(R.mipmap.man);
            } else if (coh.getUserNumber(fname) == 5) {
                userpic.setImageResource(R.mipmap.triviausericon);
            }
        }
    }

    //Drawer function - goes to the right page accordingly(Just like the menu)
    public void clickDrawer() {
        //Like onOptionItemSelected just in drawer
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.item_pg1) {
                    movetopageHome();
                }
                if (id == R.id.item_pg2) {
                    movetopageBoard();
                }
                if (id == R.id.profile) {
                    movetoprofile();
                }
                if (id == R.id.logout) {
                    logoutuser();
                }
                if (id == R.id.item_exit) {
                    alertDialogExit();
                }

                drawerLayout.closeDrawer(GravityCompat.START);

                return true;
            }
        });
    }

    //Refresh the activity
    @Override
    protected void onResume() {
        super.onResume();
        updateHeader();
    }
}
