package com.example.triviaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Howtoplay extends AppCompatActivity {
    //how to play page

    ImageView imageAnime;//animation moving pictures

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_howtoplay);
        imageAnime = findViewById(R.id.imgshow);//get xml info
        setTitle("Instructions");

        //left to right page
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        AnimationDrawable animation = new AnimationDrawable();


        animation.addFrame(getDrawable(R.mipmap.sportico),800);
        animation.addFrame(getDrawable(R.mipmap.scienceico), 800);
        animation.addFrame(getDrawable(R.mipmap.animalsico), 800);
        animation.addFrame(getDrawable(R.mipmap.geographyico), 800);
        animation.addFrame(getDrawable(R.mipmap.foodico), 800);
        animation.addFrame(getDrawable(R.mipmap.tvico), 800);
        animation.setOneShot(false);//infinite loop
        imageAnime.setBackground(animation);

        //start the animation!
        animation.start();

    }

    //button that goes back(closes current activity)
    public void back(View view) {
        finish();
        //nice transition
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}