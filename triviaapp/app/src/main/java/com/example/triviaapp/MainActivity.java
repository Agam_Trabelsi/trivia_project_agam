package com.example.triviaapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    //home page

    Intent musicIntnet;//access to music service
    ImageView imageView, imageView2;//animation for question marks
    ImageButton sound; //Declaration type --> Image button

    Boolean musicon;//picture shared preference info (bool val)
    SharedPreferences sp2;//contains current music state the may have changed due to usage in play button in loginorsign page

    //Drawer's stuff
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ImageView userpic;
    userDataBase coh;

    AirplaneModeChangeReceiver airplaneModeChangeReceiver = new AirplaneModeChangeReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //here we start seeing the action bar
        setTitle("Home Page");//sets headline to home page on the top of the page

        //connecting question marks images to xml
        imageView = (ImageView) findViewById(R.id.bg);
        imageView2 = (ImageView) findViewById(R.id.bg2);
        sound = (ImageButton) findViewById(R.id.musicbtn);


        //makes sure the page is in the right direction (usually unneeded)
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        //getting the music info from the service
        musicIntnet = new Intent(this, MyService.class);


        // Retrieve the shared preferences instance for "details2" with private access mode
        sp2 = getApplicationContext().getSharedPreferences("details2", Context.MODE_PRIVATE);
        // Retrieve the boolean value associated with the key "music" from shared preferences
        musicon = sp2.getBoolean("music", true);
        // If music is enabled, start the music service and set the volume up icon
        if (musicon)
        {
            startService(musicIntnet);
            sound.setImageResource(R.drawable.ic_baseline_volume_up_24);
        } else
        {
            // If music is disabled, stop the music service and set the volume off icon
            stopService(musicIntnet);
            sound.setImageResource(R.drawable.ic_baseline_volume_off_24);
        }


        //Make all the preparation for the drawer
        prepDrawer();

        //Calling to function that update the header of
        updateHeader();

        //Make the Drawer clickable like the menu
        clickDrawer();


        //the next lines allow the animation
        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);//starts the animation for one second
        animator.setRepeatCount(ValueAnimator.INFINITE);//takes this second and makes it infinite
        animator.setInterpolator(new LinearInterpolator());//like Interpolation, makes the transition in a constant pace(linear)
        animator.setDuration(10000L);//a lot of time so it doesn't end (10 seconds, and repeats)
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();//gets the info from before
                final float width = imageView.getWidth();//gets picture width
                final float translationX = width * progress; //transition of x using the size of the page and the process itself
                imageView.setTranslationX(translationX);//anim on first image
                imageView2.setTranslationX(translationX - width);//anim for the second one
                //2 pics - so it won't be empty and move as one

            }
        });
        animator.start();//starts animation


    }

    //creating a menu
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    //menu function - goes to the right page accordingly
    public boolean onOptionsItemSelected(final MenuItem item) {
        //all the id's are from the menu xml
        int id = item.getItemId();
        //Now that our menu in the drawer we only need to show the burger icon and make in clickable with this small if
        if (id == android.R.id.home) {
            //If open show the open icon(arrow icon)
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                //Else show the close icon (burger icon)
                drawerLayout.openDrawer(GravityCompat.START);
            }
            return true;
        }
        return true;
    }

    public void alertDialogExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);//creates a new dialog
        builder.setTitle("Exit app");//headline
        builder.setIcon(R.mipmap.exitapp);//icon on headline
        builder.setMessage("Are you sre you want to exit?");
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() //the answer yes
        {
            public void onClick(DialogInterface dialog, int which)
            {
                //changing music to initial state
                SharedPreferences.Editor editor = sp2.edit();
                editor.putBoolean("music", true);
                editor.apply();
                dialog.dismiss();//closing dialog
                Toast.makeText(MainActivity.this, "Goodbye!", Toast.LENGTH_LONG).show();
                stopService(musicIntnet);
                finish();//closing the page
                finishAffinity();//closing the entire app
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() //choosing no
        {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();//closes dialog only, not app
                Toast.makeText(MainActivity.this, "enjoy playing!", Toast.LENGTH_LONG).show();
            }
        });
        AlertDialog alertDialog = builder.create();//all we did was in theory, now the creation of the window happens
        alertDialog.show();//showing the dialog on screen
    }


    //logout function and takes user back to before the login
    public void logoutuser() {
        //disconnecting all shared preference because current user is disconnecting from the app(clearing for the next user)
        SharedPreferences preferences = getSharedPreferences("details1", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        SharedPreferences score = getSharedPreferences("Score", Context.MODE_PRIVATE);
        SharedPreferences.Editor edscore = score.edit();
        edscore.clear();
        edscore.commit();
        Intent intent = new Intent(this, logorsign.class);
        startActivity(intent);
    }

    //this function is responsible for the music (turning it on and off)
    public void tostartmusic(View view) {
        //according to the button's state, the music starts or stops(change in state).
        if (musicon == false) {
            startService(musicIntnet);
            musicon = true;
            sound.setImageResource(R.drawable.ic_baseline_volume_up_24);
        } else {
            stopService(musicIntnet);
            musicon = false;
            sound.setImageResource(R.drawable.ic_baseline_volume_off_24);
        }
        //Save the current music state to SharedPreferences
        SharedPreferences.Editor editor = sp2.edit();
        editor.putBoolean("music", musicon);
        editor.apply();

    }

    //goes to start - in order to start the game
    public void startgame(View view) {
        Intent intent = new Intent(MainActivity.this, CategoryActivity.class);
        startActivity(intent);
        //a nice transition
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    //how to play button - goes to instructions
    public void howtoplay(View view) {
        Intent intent = new Intent(MainActivity.this, Howtoplay.class);
        startActivity(intent);
        //a nice transition
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    //Functions to move between intents
    public void movetopageHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void movetopageBoard() {
        Intent intent = new Intent(this, LeaderBoard.class);
        startActivity(intent);
    }

    public void movetoprofile() {
        Intent intent = new Intent(this, userprofile.class);
        startActivity(intent);
    }

    //To make the code more clear(In some way) instead of put all of this in onCreate we make function that take care for this mess.
    //for the menu icon - to know if it is open or closed and creating all of it
    public void prepDrawer()
    {
        coh = new userDataBase(this);
        //Declaring about Drawer verbs
        ActionBar actionBar = getSupportActionBar();//action bar with the 3 lines of the menu
        if (actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_baseline_menu_24);//Creating the burger icon at the menu
        }
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.navview);
        //used to create a hamburger icon in the action bar that is associated with a navigation drawer
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);//listener that knows if the menu is open or not
        actionBarDrawerToggle.syncState();//sets menu icon pic according to manu state(close or open)
    }

    //Update the header of the Drawer
    public void updateHeader() {
        Intent intent = getIntent();
        SharedPreferences sp = getApplicationContext().getSharedPreferences("details1", Context.MODE_PRIVATE);
        String fname = sp.getString("fname", "");//gets the user name that is saved in the shared preference in order to display it in the menu
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = (TextView) headerView.findViewById(R.id.tvusername);
        navUsername.setText("Hello " + fname);
        userpic = (ImageView) headerView.findViewById(R.id.userpic);
        if ((coh.getPic(Long.parseLong(coh.getIdbyName(fname)))) != null)//no pic in database
        {
            if (coh.getUserNumber(fname) == 0) {
                userpic.setImageBitmap((coh.getPic(Long.parseLong(coh.getIdbyName(fname)))));
            } else if (coh.getUserNumber(fname) == 1) {
                userpic.setImageResource(R.mipmap.boy);
            } else if (coh.getUserNumber(fname) == 2) {
                userpic.setImageResource(R.mipmap.girl);
            } else if (coh.getUserNumber(fname) == 3) {
                userpic.setImageResource(R.mipmap.women);
            } else if (coh.getUserNumber(fname) == 4) {
                userpic.setImageResource(R.mipmap.man);
            } else if (coh.getUserNumber(fname) == 5) {
                userpic.setImageResource(R.mipmap.triviausericon);
            }
        }
        else
        {
            if (coh.getUserNumber(fname) == 1) {
                userpic.setImageResource(R.mipmap.boy);
            } else if (coh.getUserNumber(fname) == 2) {
                userpic.setImageResource(R.mipmap.girl);
            } else if (coh.getUserNumber(fname) == 3) {
                userpic.setImageResource(R.mipmap.women);
            } else if (coh.getUserNumber(fname) == 4) {
                userpic.setImageResource(R.mipmap.man);
            } else if (coh.getUserNumber(fname) == 5) {
                userpic.setImageResource(R.mipmap.triviausericon);
            }
        }
    }

    //Drawer function - goes to the right page accordingly(Just like the menu)
    public void clickDrawer() {
        //Like onOptionItemSelected just in drawer
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {//so it knows what i chose on the menu it listens to it
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.item_pg1) {
                    movetopageHome();
                }
                if (id == R.id.item_pg2) {
                    movetopageBoard();
                }
                if (id == R.id.profile) {
                    movetoprofile();
                }
                if (id == R.id.logout) {
                    logoutuser();
                }
                if (id == R.id.item_exit) {
                    alertDialogExit();
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    //Refresh the activity
    @Override
    protected void onResume() {
        super.onResume();
        updateHeader();
    }

    //control the broadcast receiver and know if airplane mode is on or off
    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        registerReceiver(airplaneModeChangeReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(airplaneModeChangeReceiver);
    }
}