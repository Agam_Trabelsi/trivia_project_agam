package com.example.triviaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import java.io.ByteArrayOutputStream;
//this class helps us with converting bitmap to byte arrays - so we can save it in the database
public class Helper {
    // the function gets a bitmap object and converts it to bit array
    public static byte[] bitmapToByteArray(Bitmap in) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        in.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        return bytes.toByteArray();

    }

    //opposite - byte array back to bitmap
    public static Bitmap byteArrayToBitmap(byte[] bytes) {
        if (bytes != null) {
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        } else {
            return null;
        }
    }
}

