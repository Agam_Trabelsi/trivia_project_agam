package com.example.triviaapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.transition.Fade;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

public class QuestionActivity extends AppCompatActivity implements View.OnClickListener {
    //the actual game page - questions sets with timer

    private CountDownTimer countDown;//timer object
    TextView txtQuestion, timer, totalqust;
    Button q1, q2, q3, q4;
    ProgressBar mProgressBar;//white line behind timer
    boolean hasStarted = false;
    ImageView catimg;//category img on the side

    String subject;
    int score = 0;
    int totalQustion = 10;
    int currentQuestionIndex = 0;
    String selectedAnswer = "";
    String trueAnswer = "";
    private CategoryModel CategoryModel;
    CardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        //page presented left to right
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        //connection to xml
        mProgressBar = (ProgressBar) findViewById(R.id.pr);
        catimg = (ImageView) findViewById(R.id.catimge);
        cardView = (CardView) findViewById(R.id.cardcat);
        txtQuestion = (TextView) findViewById(R.id.question);
        totalqust = (TextView) findViewById(R.id.quest_num);
        timer = (TextView) findViewById(R.id.countdown);
        q1 = (Button) findViewById(R.id.option1);
        q2 = (Button) findViewById(R.id.option2);
        q3 = (Button) findViewById(R.id.option3);
        q4 = (Button) findViewById(R.id.option4);

        //creating listeners to all answer buttons
        q1.setOnClickListener(this);
        q2.setOnClickListener(this);
        q3.setOnClickListener(this);
        q4.setOnClickListener(this);

        totalqust.setText(currentQuestionIndex + "/" + totalQustion);

        Intent intent = getIntent();
        if (intent.getExtras() != null)//we got the category pic with get extra from the category page
        {
            CategoryModel = (CategoryModel) intent.getSerializableExtra("data");

            //using getters to extract info from the object category model
            int image = CategoryModel.getImage();
            String name = CategoryModel.getName();

            setTitle(name);//name of page according to chosen category
            catimg.setImageResource(image);//sets pic on the side
            subject = CategoryModel.getName();
            //we need category color cause it changes in different runs do to random
            Bundle extras = getIntent().getExtras();
            int color = extras.getInt("color");
            cardView.setCardBackgroundColor(color);//sets the color to be that

            Log.e("PASSED ", "====> " + CategoryModel.getName());//if there is a problem lets the programmer know
        }


        //load a new question and starts the timer
        loadNewQustion();
        startTimer();

    }

    //loads questions according to category
    void loadNewQustion()
    {
        totalqust.setText(currentQuestionIndex + "/" + totalQustion);
        if (hasStarted)//first time false
        {
            //restarts timer
            countDown.cancel();
            timer.setText("10");
            mProgressBar.setProgress(10);
            startTimer();
        }

        //checks if everything is done
        if (currentQuestionIndex == totalQustion)
        {
            finishQuiz();//ending quiz
            return;
        }

        //gets info to the buttons and question
        if (subject.equals("Sports"))
        {
            txtQuestion.setText(QSportSet.question[currentQuestionIndex]);
            q1.setText(QSportSet.choices[currentQuestionIndex][0]);
            q2.setText(QSportSet.choices[currentQuestionIndex][1]);
            q3.setText(QSportSet.choices[currentQuestionIndex][2]);
            q4.setText(QSportSet.choices[currentQuestionIndex][3]);
        }
        else if (subject.equals("Science")) {
            txtQuestion.setText(QScienceSet.question[currentQuestionIndex]);
            q1.setText(QScienceSet.choices[currentQuestionIndex][0]);
            q2.setText(QScienceSet.choices[currentQuestionIndex][1]);
            q3.setText(QScienceSet.choices[currentQuestionIndex][2]);
            q4.setText(QScienceSet.choices[currentQuestionIndex][3]);
        } else if (subject.equals("Geography")) {
            txtQuestion.setText(QGeographySet.question[currentQuestionIndex]);
            q1.setText(QGeographySet.choices[currentQuestionIndex][0]);
            q2.setText(QGeographySet.choices[currentQuestionIndex][1]);
            q3.setText(QGeographySet.choices[currentQuestionIndex][2]);
            q4.setText(QGeographySet.choices[currentQuestionIndex][3]);
        } else if (subject.equals("Animals")) {
            txtQuestion.setText(QAnimalsSet.question[currentQuestionIndex]);
            q1.setText(QAnimalsSet.choices[currentQuestionIndex][0]);
            q2.setText(QAnimalsSet.choices[currentQuestionIndex][1]);
            q3.setText(QAnimalsSet.choices[currentQuestionIndex][2]);
            q4.setText(QAnimalsSet.choices[currentQuestionIndex][3]);
        } else if (subject.equals("Entertainment")) {
            txtQuestion.setText(QEntertainmentSet.question[currentQuestionIndex]);
            q1.setText(QEntertainmentSet.choices[currentQuestionIndex][0]);
            q2.setText(QEntertainmentSet.choices[currentQuestionIndex][1]);
            q3.setText(QEntertainmentSet.choices[currentQuestionIndex][2]);
            q4.setText(QEntertainmentSet.choices[currentQuestionIndex][3]);
        } else if (subject.equals("Food & drinks")) {
            txtQuestion.setText(QFooddrinksSet.question[currentQuestionIndex]);
            q1.setText(QFooddrinksSet.choices[currentQuestionIndex][0]);
            q2.setText(QFooddrinksSet.choices[currentQuestionIndex][1]);
            q3.setText(QFooddrinksSet.choices[currentQuestionIndex][2]);
            q4.setText(QFooddrinksSet.choices[currentQuestionIndex][3]);
        }


        //gets all buttons back to blue(correct is green an incorrect red - we want to reset them as soon an we start another question)
        q1.setBackground(getDrawable(R.drawable.round_corner));
        q2.setBackground(getDrawable(R.drawable.round_corner));
        q3.setBackground(getDrawable(R.drawable.round_corner));
        q4.setBackground(getDrawable(R.drawable.round_corner));
        q1.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorStateEnabled));
        q2.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorStateEnabled));
        q3.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorStateEnabled));
        q4.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorStateEnabled));

    }

    //starting the timer
    private void startTimer()
    {
        countDown = new CountDownTimer(11000, 1000)//1 second in between transition
            //this is like a listener
        {
            @Override
            //every tick in the clock does the following tasks
            public void onTick(long millisUntilFinished)
            {
                if (millisUntilFinished < 10000)//still ticking
                {
                    hasStarted = true;
                    timer.setText(String.valueOf(millisUntilFinished / 1000));//number of seconds left calculation
                    mProgressBar.setProgress((int) (millisUntilFinished / 1000));//progress bar same thing

                }
            }

            //times up or moves question
            @Override
            public void onFinish()
            {
                countDown.start();
                currentQuestionIndex++;
                totalqust.setText(currentQuestionIndex + "/" + totalQustion);
                loadNewQustion();//calling each other
            }
        };
        countDown.start();

    }

    //clicking answer button
    @Override
    public void onClick(View view)
    {
        Button clickButton = (Button) view;
        selectedAnswer = clickButton.getText().toString();//texts of chosen answer

        //checks if the answer is correct for each category
        //Category 1
        if (subject.equals("Sports"))
        {
            String currect;
            if (clickButton.getId() == R.id.option1 || clickButton.getId() == R.id.option2 || clickButton.getId() == R.id.option3 || clickButton.getId() == R.id.option4) //clicked 1 answer
            {
                if (selectedAnswer.equals(QSportSet.correctAnswers[currentQuestionIndex]))//if what the user chose is the correct answer
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.rightasnwer));//color of the button is now green
                    disabletouch();//cant be pressed again
                    score++;//adds point to final score
                }
                else//user is incorrect
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.worngasnwer));//his answer will be red
                    correctAnswer(q1, q2, q3, q4, QSportSet.correctAnswers[currentQuestionIndex]);//the correct one will be green
                    disabletouch();//cant be pressed again
                }
                //half a second delay to show the answer of the set
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        // Do something after 0.5s = 500ms
                        currentQuestionIndex++;//adds since we move on a question
                        totalqust.setText(currentQuestionIndex + "/" + totalQustion);
                        loadNewQustion();//loads a question
                        enabletouch();//now can be pressed
                    }
                }, 500);
            }
        }

        //Category 2
        else if (subject.equals("Science")) {
            if (clickButton.getId() == R.id.option1 || clickButton.getId() == R.id.option2 || clickButton.getId() == R.id.option3 || clickButton.getId() == R.id.option4)
            {
                if (selectedAnswer.equals(QScienceSet.correctAnswers[currentQuestionIndex]))
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.rightasnwer));
                    disabletouch();
                    score++;
                }
                else
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.worngasnwer));
                    correctAnswer(q1, q2, q3, q4, QScienceSet.correctAnswers[currentQuestionIndex]);
                    disabletouch();
                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 0.5s = 500ms
                        currentQuestionIndex++;
                        totalqust.setText(currentQuestionIndex + "/" + totalQustion);
                        loadNewQustion();
                        enabletouch();
                    }
                }, 500);
            }
        }

        //Category 3
        else if (subject.equals("Geography"))
        {
            if (clickButton.getId() == R.id.option1 || clickButton.getId() == R.id.option2 || clickButton.getId() == R.id.option3 || clickButton.getId() == R.id.option4)
            {
                if (selectedAnswer.equals(QGeographySet.correctAnswers[currentQuestionIndex]))
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.rightasnwer));
                    disabletouch();
                    score++;
                }
                else
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.worngasnwer));
                    correctAnswer(q1, q2, q3, q4, QGeographySet.correctAnswers[currentQuestionIndex]);
                    disabletouch();
                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 0.5s = 500ms
                        currentQuestionIndex++;
                        totalqust.setText(currentQuestionIndex + "/" + totalQustion);
                        loadNewQustion();
                        enabletouch();
                    }
                }, 500);
            }

        }

        //Category 4
        else if (subject.equals("Animals"))
        {
            if (clickButton.getId() == R.id.option1 || clickButton.getId() == R.id.option2 || clickButton.getId() == R.id.option3 || clickButton.getId() == R.id.option4)
            {
                if (selectedAnswer.equals(QAnimalsSet.correctAnswers[currentQuestionIndex]))
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.rightasnwer));
                    disabletouch();
                    score++;
                }
                else
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.worngasnwer));
                    correctAnswer(q1, q2, q3, q4, QAnimalsSet.correctAnswers[currentQuestionIndex]);
                    disabletouch();
                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 0.5s = 500ms
                        currentQuestionIndex++;
                        totalqust.setText(currentQuestionIndex + "/" + totalQustion);
                        loadNewQustion();
                        enabletouch();
                    }
                }, 500);
            }

        }

        //Category 5
        else if (subject.equals("Entertainment"))
        {
            if (clickButton.getId() == R.id.option1 || clickButton.getId() == R.id.option2 || clickButton.getId() == R.id.option3 || clickButton.getId() == R.id.option4)
            {
                if (selectedAnswer.equals(QEntertainmentSet.correctAnswers[currentQuestionIndex]))
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.rightasnwer));
                    disabletouch();
                    score++;
                }
                else
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.worngasnwer));
                    correctAnswer(q1, q2, q3, q4, QEntertainmentSet.correctAnswers[currentQuestionIndex]);
                    disabletouch();
                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 0.5s = 500ms
                        currentQuestionIndex++;
                        totalqust.setText(currentQuestionIndex + "/" + totalQustion);
                        loadNewQustion();
                        enabletouch();
                    }
                }, 500);
            }

        }

        //Category 6
        else if (subject.equals("Food & drinks"))
        {
            if (clickButton.getId() == R.id.option1 || clickButton.getId() == R.id.option2 || clickButton.getId() == R.id.option3 || clickButton.getId() == R.id.option4)
            {
                if (selectedAnswer.equals(QFooddrinksSet.correctAnswers[currentQuestionIndex]))
                {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.rightasnwer));
                    disabletouch();
                    score++;
                } else {
                    clickButton.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.worngasnwer));
                    correctAnswer(q1, q2, q3, q4, QFooddrinksSet.correctAnswers[currentQuestionIndex]);
                    disabletouch();
                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 0.5s = 500ms
                        currentQuestionIndex++;
                        totalqust.setText(currentQuestionIndex + "/" + totalQustion);
                        loadNewQustion();
                        enabletouch();
                    }
                }, 500);
            }

        }
    }

    // saves quiz results and moves to next page
    public void finishQuiz()
    {
        String passStatus = "";
        //at least 6 questions but not all of them
        if (score >= 6 && score != totalQustion)
        {
            passStatus = "Good job!";
        }
        else if (score == totalQustion)//all correct
        {
            passStatus = "Excellent work!";
        }
        else//5 and down
        {
            passStatus = "You can do better!";
        }
        //close timer (bugs control)

        if (hasStarted)
        {
            countDown.cancel();
        }

        //put extra for next page (score)
        Intent intent = new Intent(QuestionActivity.this, ScoreActivity.class);
        intent.putExtra("point", score);
        intent.putExtra("status", passStatus);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);//transition

    }

    //prevents the users ability to double click the button, that way it doesn't skip questions
    public void disabletouch()
    {
        q1.setEnabled(false);
        q2.setEnabled(false);
        q3.setEnabled(false);
        q4.setEnabled(false);
    }

    //make the button clickable again
    public void enabletouch()
    {
        q1.setEnabled(true);
        q2.setEnabled(true);
        q3.setEnabled(true);
        q4.setEnabled(true);
    }

    public void correctAnswer(Button q1, Button q2, Button q3, Button q4, String trueanswer) {
        if (q1.getText().equals(trueanswer))
        {
            q1.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.rightasnwer));
        } else if (q2.getText().equals(trueanswer))
        {
            q2.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.rightasnwer));
        } else if (q3.getText().equals(trueanswer)) {
            q3.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.rightasnwer));
        } else if (q4.getText().equals(trueanswer)) {
            q4.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.rightasnwer));
        }
    }


    //destructor to timer when we finish its work
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        countDown.cancel();
    }
}