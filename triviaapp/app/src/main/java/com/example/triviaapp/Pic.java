package com.example.triviaapp;
import android.graphics.Bitmap;

//creating a class with bitmap object in it that will help with saving pictures in the database
public class Pic
{
    private Bitmap bitmap;//declaring a bitmap object

    //constructor
    public Pic(Bitmap bitmap)
    {
        this.bitmap = bitmap;
    }

    //getter
    public Bitmap getBitmap()
    {
        return bitmap;
    }

    //setter
    public void setBitmap(Bitmap bitmap)
    {
        this.bitmap = bitmap;
    }
}
