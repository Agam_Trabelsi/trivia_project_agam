package com.example.triviaapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.rpc.Help;

import java.util.Calendar;

public class userprofile extends AppCompatActivity implements View.OnClickListener, SensorEventListener {
    //page that contains data on the user

    //declaration
    TextView tv, scoretv, tvTemp;
    ImageButton img;
    Bitmap bitmap;
    userDataBase db, coh;
    Pic d;
    Button btnsave, btnapp;
    private SensorManager sensormanager;//access to sensors
    private Sensor temperature;//a variable of type sensor

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userprofile);

        //connecting to activity
        tv = findViewById(R.id.usernameid);
        scoretv = findViewById(R.id.userscore);
        tvTemp = findViewById(R.id.phonetemp);
        img = (ImageButton) findViewById(R.id.userpic);
        btnapp = (Button) findViewById(R.id.btntoapp);
        btnsave = (Button) findViewById(R.id.savebtn);

        //set listeners to the buttons
        btnapp.setOnClickListener(this);
        btnsave.setOnClickListener(this);
        img.setOnClickListener(this);

        //get info from details1 - that has the username of the user connected
        SharedPreferences sp = getApplicationContext().getSharedPreferences("details1", Context.MODE_PRIVATE);
        String fname = sp.getString("fname", "");

        //with the info from sp we are welcoming the connected user
        tv.setText("Welcome " + fname);

        d = new Pic(bitmap);//picture object - bitmap (empty now)
        coh = new userDataBase(this);//creating an object that will be used to access the data base

        setTitle("User Profile");//page title

        //page set left to right
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)//checks if the version of the Android operating system running on the device is equal to or greater than JELLY_BEAN_MR1 (Android 4.2, API level 17).
        {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

        //another access to data base
        db = new userDataBase(this);

        if ((db.getPic(Long.parseLong(db.getIdbyName(fname)))) != null) //our user took a picture at least once
        {
            if (coh.getUserNumber(fname) == 0)
            {
                img.setImageBitmap((db.getPic(Long.parseLong(db.getIdbyName(fname)))));
            }
            else if (coh.getUserNumber(fname) == 1)
            {
                img.setImageResource(R.mipmap.boy);
            }
            else if (coh.getUserNumber(fname) == 2)
            {
                img.setImageResource(R.mipmap.girl);
            }
            else if (coh.getUserNumber(fname) == 3)
            {
                img.setImageResource(R.mipmap.women);
            }
            else if (coh.getUserNumber(fname) == 4)
            {
                img.setImageResource(R.mipmap.man);
            }
            else if (coh.getUserNumber(fname) == 5)
            {
                img.setImageResource(R.mipmap.triviausericon);
            }
        }
        else //only used icons so redundant to check for pic
        {
            if (coh.getUserNumber(fname) == 1)
            {
                img.setImageResource(R.mipmap.boy);
            }
            else if (coh.getUserNumber(fname) == 2)
            {
                img.setImageResource(R.mipmap.girl);
            }
            else if (coh.getUserNumber(fname) == 3)
            {
                img.setImageResource(R.mipmap.women);
            }
            else if (coh.getUserNumber(fname) == 4)
            {
                img.setImageResource(R.mipmap.man);
            }
            else if (coh.getUserNumber(fname) == 5)
            {
                img.setImageResource(R.mipmap.triviausericon);
            }
        }

        //allscores = total sum scores of user

        if (db.getAllScores(Long.parseLong(db.getIdbyName(fname))) == null) //hasn't played yet
        {
            scoretv.setText("0");//score must be 0 and that will be presented on the screen
        }
        else //played
        {
            scoretv.setText(db.getAllScores(Long.parseLong(db.getIdbyName(fname))));//show the total user score that is in the data base
        }

        //access to sensors
        sensormanager = (SensorManager) getSystemService(SENSOR_SERVICE);
        temperature = sensormanager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);

        //checks if the device supports this kind of sensor
        if (temperature != null)//has info - the temperature
            tvTemp.setText("Temp sensor: " + temperature.getPower());//device supports this sensor
        else
            tvTemp.setText("Your device doesn't have a temperature sensor");//no info in variable so it means the device doesn't support this kind of sensor

    }

    @Override
    //buttons pressed
    public void onClick(View v)
    {
        if (btnapp == v)//the button that closes the page was pushed
        {
            finish();//closes current activity
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);//transition animation
        }

        if (btnsave == v)//the button that takes picture
        {
            Intent intent2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent2, 0);//this function works - related to the app version (written in previous version)
            //opens camera and goes to onActivityResult function! - saves pic in both databases

            if (bitmap == null)//picture was not taken or not saved
            {
                bitmap = (BitmapFactory.decodeResource(getResources(), R.mipmap.triviausericon));//set default icon
            }
            else//picture take and will be the users picture
            {
                img.setImageBitmap(bitmap);
            }
        }
        //press the circle gives option to choose avatar
        if (img == v)
        {
            openDialog();
        }
    }

    //called from start activity result - saves pic in database
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0)//only 1 request was made with this code - camera. it relates to the start activity function
        {
            if (resultCode == RESULT_OK)//the user took a picture and confirmed it to be his profile pic with the tic
            {
                bitmap = (Bitmap) data.getExtras().get("data");//the data is the picture that was taken by the user
                img.setImageBitmap(bitmap);//sets it as the image

                //gets user name from sp
                SharedPreferences sp = getApplicationContext().getSharedPreferences("details1", Context.MODE_PRIVATE);
                String fname = sp.getString("fname", "");

                Pic pic = new Pic(bitmap);//pic object with the photo that was taken

                if (pic.getBitmap() != null)//pic taken
                {
                    long id = coh.addPictoProf(pic, fname);//add to db1
                    coh.updateUserNumber(fname, 0);//this number is of a picture not avatar
                    if (coh.getProfilesCount() > 0)//app has users
                    {
                        long id2 = coh.addPictoProfuser(pic, fname);//add to db2 (leader board)
                        coh.updateUsertableNumber(fname, 0);//number pic for leader board
                    }
                }
            }
        }
        else
        {
            img.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.triviausericon));//if the request code is accidentally something else leave it as the default icon
        }
    }


    // temperature sensor functions
    protected void onResume()
    {
        super.onResume();
        sensormanager.registerListener(this, temperature, SensorManager.SENSOR_DELAY_FASTEST);//listener
    }

    protected void onPause()
    {
        super.onPause();
        sensormanager.unregisterListener(this);
    }

    @Override
    //listener
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        if (sensorEvent.sensor.getType() != Sensor.TYPE_AMBIENT_TEMPERATURE)
        {
            return;
        }

        if (temperature != null)
        {
            tvTemp.setText("Temp sensor: " + temperature.getPower());
        }
        else
            tvTemp.setText("Sorry - your device doesn't have an ambient temperature sensor");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i)
    {

    }

    //user deletes themselves - activated when the button is pushed
    public void removeuser(View view)
    {
        //opens dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(userprofile.this);
        builder.setTitle("Delete the user");//headline of dialog
        builder.setIcon(R.mipmap.triviausericon);//dialog icon(app icon)
        builder.setMessage("Are you sre you want to delete and remove all the data of the current user?");//question to user
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() //if wants to delete opens listener
        {
            public void onClick(DialogInterface dialog, int which)
            {
                SharedPreferences sp = getApplicationContext().getSharedPreferences("details1", Context.MODE_PRIVATE);
                String fname = sp.getString("fname", "");//gets name from sp

                coh.deleteUser(fname);//deleted user from both db
                Intent intent = new Intent(userprofile.this, logorsign.class);//logs out
                startActivity(intent);
                finish();//closes window
                finishAffinity();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() //doesn't want to delete user - opens listener:
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();//closes the dialog and does nothing
            }
        });
        AlertDialog alertDialog = builder.create();//here its created
        alertDialog.show();//showing it to user
    }




    //creating costumed avatar dialog
    public void openDialog()
    {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar);//no builder means costumed
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//no title to dialog
        dialog.setContentView(R.layout.dialog);//the design created in dialog.xml

        SharedPreferences sp = getApplicationContext().getSharedPreferences("details1", Context.MODE_PRIVATE);
        String fname = sp.getString("fname", "");//gets user name from sp

        dialog.getWindow().getAttributes().windowAnimations = R.style.Dialog;//fade in and out - info from themes

        //connecting all buttons to their xml view
        ImageButton boy = (ImageButton) dialog.findViewById(R.id.boyav);
        ImageButton girl = (ImageButton) dialog.findViewById(R.id.girlav);
        ImageButton man = (ImageButton) dialog.findViewById(R.id.manlav);
        ImageButton woman = (ImageButton) dialog.findViewById(R.id.womanlav);
        ImageButton def = (ImageButton) dialog.findViewById(R.id.defaultlav);
        ImageButton back = (ImageButton) dialog.findViewById(R.id.btnback);//red X

        //listener for each button
        //updates pic number in db
        boy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //update in both db
                coh.updateUserNumber(fname, 1);
                coh.updateUsertableNumber(fname, 1);
                recreate();//the pic will be set right away - we don't need to exit the page and return to it to see the change (like refresh) - recreation of the page
                dialog.dismiss();//close dialog

            }
        });

        girl.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                coh.updateUserNumber(fname, 2);
                coh.updateUsertableNumber(fname, 2);
                recreate();
                dialog.dismiss();

            }
        });

        woman.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                coh.updateUserNumber(fname, 3);
                coh.updateUsertableNumber(fname, 3);
                recreate();
                dialog.dismiss();

            }
        });

        man.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                coh.updateUserNumber(fname, 4);
                coh.updateUsertableNumber(fname, 4);
                recreate();
                dialog.dismiss();
            }
        });

        //default = def
        def.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                coh.updateUserNumber(fname, 5);
                coh.updateUsertableNumber(fname, 5);
                recreate();
                dialog.dismiss();
            }
        });

        //just closes dialog (red X)
        back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();

            }
        });
        dialog.show();//present dialog
    }
}
