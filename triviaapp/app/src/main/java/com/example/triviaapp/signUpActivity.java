package com.example.triviaapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class signUpActivity extends AppCompatActivity {
    //signup page


    private Context context;//connection to activity
    private EditText userName, password;//the text boxes that the user puts their information in

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //these lines hide the action bar and create a full screen, the action bar will reappear on homepage
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);


        //to get on the safe side, if the page flips, it doesn't, this shouldn't be working at all
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL)
        {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

        context = this;//saving current activity

        //connecting the edit text to xml
        userName = findViewById(R.id.newUsername);
        password = findViewById(R.id.newPassword);
    }

    //when pressing the signup button it signs up a new user using this function
    public void signUpClick(View view)
    {
        //variable that gives access to database
        userDataBase myUser = new userDataBase(signUpActivity.this);
        //text boxes are empty
        if ((userName.getText().length() > 0) && (password.getText().toString().length()>0))
        {
            //checking if the user exists and if not creates it
            if(myUser.doesExist(userName.getText().toString()) == 0)//checks with user name
            {
                //uses database functions and creates a user with the info from edit text and 0 points
                myUser.insertUser(userName.getText().toString(), password.getText().toString());
                myUser.addScoreusernew(String.valueOf(0), userName.getText().toString());
                //going to login page
                Intent intent = new Intent(this, activity_login.class);
                startActivity(intent);
            }
            //this user name exists, you may be logged in or you used someones username
            else
            {
                if(myUser.doesPasswordExist(password.getText().toString()) == 0)
                {
                    Toast.makeText(context, "Username exists! try a different one", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(context, "You already have a user! go to login", Toast.LENGTH_SHORT).show();
                }
            }
        }

        //at least 1 text box is empty and it gives the right message
        else
        {
            if ((userName.getText().length() == 0) && (password.getText().toString().length() ==0))
            {
                Toast.makeText(context, "Fields are empty!", Toast.LENGTH_SHORT).show();
            }
            else if(userName.getText().length() == 0)
            {
                Toast.makeText(context, "You forgot the username!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "You forgot the password!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //going to login page when clicking the login button
    public void tologinClick(View view) {
        Intent intent = new Intent(this, activity_login.class);
        startActivity(intent);//moving to login page
        //another use of animation that makes a smoother transition for a better user experience(blink)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

}