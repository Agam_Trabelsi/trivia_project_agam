package com.example.triviaapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.graphics.Color;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class LeaderBoard extends AppCompatActivity {


    private ListView listView;//users list
    private SimpleCursorAdapter adapter;
    private SQLiteDatabase database;//the database
    userDataBase coh;//both data bases


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);
        setTitle("Leaderboard ");//title page

        //makes sure the page is in the right direction (usually unneeded)
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        // connection to database
        coh = new userDataBase(this);


        // this will show the database in a ui form
        listView = findViewById(R.id.LeaderList);

        // open the database
        database = openOrCreateDatabase("APP37.db", MODE_PRIVATE, null);

        // placing cursor on the first result
        Cursor cursor = database.rawQuery("SELECT user, score, _id, userimage,usernumber FROM ScoreTable ORDER BY CAST(score AS INTEGER) DESC", null);//gets all users relevant info for table and displays it in the right order

        // all the values needed to display relevant data
        String[] fromColumns = {"user", "score", "_id", "userimage", "usernumber"};
        int[] toViews = {R.id.player_name, R.id.player_score, R.id.player_image, R.id.rank_text_view};//one leaderboard item array


        // creating adapter for the view and design
        adapter = new SimpleCursorAdapter(this, R.layout.leaderboarditem, cursor, fromColumns, toViews, 0);

        if (adapter != null)//adapter exists
        {
            adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder()
            {
                @Override
                public boolean setViewValue(View view, Cursor cursor, int columnIndex)
                {
                    if (view.getId() == R.id.player_image)//the specific view we are on contains player image
                    {
                        ImageView imageView = (ImageView) view;
                        //gets info on the current user from the cursor
                        int imagePathColumnIndex = cursor.getColumnIndex("userimage");
                        int fnameColumnIndex = cursor.getColumnIndex("usernumber");
                        int fname = cursor.getInt(fnameColumnIndex);
                        byte[] imageData = cursor.getBlob(imagePathColumnIndex);

                        if (imageData != null && imageData.length > 0)//there is a pic
                        {
                            Bitmap image = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);//convert to bitmap
                            if (fname == 0)//taken in phone
                            {
                                // Set image from the database
                                if (imageData != null && imageData.length > 0)
                                {
                                    if (image != null)
                                    {
                                        imageView.setImageBitmap(image);
                                    }
                                }
                            }
                            //set avatars from mipmap
                            else if (fname == 1)
                            {
                                // Set boy image from mipmap
                                imageView.setImageResource(R.mipmap.boy);
                            }
                            else if (fname == 2)
                            {
                                // Set girl image from mipmap
                                imageView.setImageResource(R.mipmap.girl);

                            }
                            else if (fname == 3)
                            {
                                // Set woman image from mipmap
                                imageView.setImageResource(R.mipmap.women);

                            }
                            else if (fname == 4)
                            {
                                // Set man image from mipmap
                                imageView.setImageResource(R.mipmap.man);

                            }
                            else
                            {
                                // Set default image from mipmap
                                imageView.setImageResource(R.mipmap.triviausericon);
                            }
                        }
                        else
                        {
                            if (fname == 1)
                            {
                                // Set boy image from mipmap
                                imageView.setImageResource(R.mipmap.boy);
                            }
                            else if (fname == 2)
                            {
                                // Set girl image from mipmap
                                imageView.setImageResource(R.mipmap.girl);
                            }
                            else if (fname == 3)
                            {
                                // Set woman image from mipmap
                                imageView.setImageResource(R.mipmap.women);
                            }
                            else if (fname == 4)
                            {
                                // Set man image from mipmap
                                imageView.setImageResource(R.mipmap.man);
                            }
                            else
                                // default pic
                                imageView.setImageResource(R.mipmap.triviausericon);
                        }
                        return true;
                    }
                    if (view.getId() == R.id.rank_text_view)
                    {
                        //coloring the ranking numbers according to the users place
                        TextView rankTextView = (TextView) view;
                        int position = cursor.getPosition();
                        rankTextView.setText(String.valueOf(position + 1));
                        //first rank is gold
                        if (position == 0)
                        {
                            rankTextView.setTextColor(getResources().getColor(R.color.firstplace));
                        }
                        //second one is silver
                        else if (position == 1)
                        {
                            rankTextView.setTextColor(getResources().getColor(R.color.secondplace));
                        }
                        //third bronze
                        else if (position == 2)
                        {
                            rankTextView.setTextColor(getResources().getColor(R.color.thiredplace));
                        }
                        //rest ugly grey
                        else
                        {
                            rankTextView.setTextColor(getResources().getColor(R.color.others));
                        }
                        return true;
                    }
                    if (view.getId() == R.id.player_name)
                    {
                        //sets background according to user connected
                        TextView nameTextView = (TextView) view;
                        String userName = cursor.getString(columnIndex);
                        nameTextView.setText(userName);

                        // gets connected name from sp
                        SharedPreferences sp = getApplicationContext().getSharedPreferences("details1", Context.MODE_PRIVATE);
                        String currentUserName = sp.getString("fname", "");

                        if (userName.equals(currentUserName))
                        {
                            // set background color of entire row to a different color
                            CardView cardView = (CardView) view.getParent().getParent();
                            cardView.setCardBackgroundColor(getResources().getColor(R.color.currentbg));
                        }
                        else
                        {
                            CardView cardView = (CardView) view.getParent().getParent();
                            cardView.setCardBackgroundColor(getResources().getColor(R.color.othersbg));
                        }

                        return true;
                    }
                    return false;
                }
            });
            adapter.notifyDataSetChanged();
        }

        listView.setAdapter(adapter);//starts its work
    }

    //goes back to home page
    public void tohome(View view)
    {
        Intent intent = new Intent(LeaderBoard.this, MainActivity.class);
        startActivity(intent);
    }
}