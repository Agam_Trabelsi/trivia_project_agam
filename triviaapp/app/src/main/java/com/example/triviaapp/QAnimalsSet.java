package com.example.triviaapp;
//questions, option answers and correct answers class (animals)
//this class has no functions only variables
public class QAnimalsSet {
    public static String question[] =
            {
                    "What is the loudest animal on Earth?",
                    "How many hearts does an octopus have?",
                    "The unicorn is the national animal of which country?",
                    "What are the folds of skin on a cat's ears called?",
                    "A group of ravens is known as?",
                    "How many legs does a spider have?",
                    "How long do elephant pregnancies last?",
                    "What mammals lay eggs?",
                    "What type of animal is a Flemish giant?",
                    "The name of which African animal means “river horse”?"
            };
    public static String choices[][] =
            {
                    {"Howler monkey", "Sperm Whale", "Mantis Shrimp", "Greater Bulldog Bat"},
                    {"3", "8", "1", "2"},
                    {"Belgium", "France", "Lithuania", "Scotland"},
                    {"Oliver's shoes", "Ava's purse", "Henry's pockets", "William's socks"},
                    {"Unkindness", "venery", "colony", "streak"},
                    {"6", "2", "4", "8"},
                    {"3 months", "9 months", "22 months", "16 months"},
                    {"Insects", "Lions", "Mammals", "Spiny anteater and the duck-billed platypus"},
                    {"Elephant", "Rabbit", "Lizard", "Whale"},
                    {"Hippopotamus", "rhinos", "leopards", "zebras"}


            };
    public static String correctAnswers[] =
            {
                    "Sperm Whale",
                    "3",
                    "Scotland",
                    "Henry's pockets",
                    "Unkindness",
                    "8",
                    "22 months",
                    "Spiny anteater and the duck-billed platypus",
                    "Rabbit",
                    "Hippopotamus"

            };
}
