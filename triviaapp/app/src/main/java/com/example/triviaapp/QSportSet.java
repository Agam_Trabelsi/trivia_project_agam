package com.example.triviaapp;

//questions, option answers and correct answers class (sport)
//this class has no functions only variables
public class QSportSet
{
    public static String question[] =
            {
                    "Which two countries have not missed one of the modern-day Olympics?",
                    "Which is the only American Football team to go a whole season undefeated, including the Super Bowl?",
                    "Who has won more tennis grand slam titles, Venus Williams or Serena Williams?",
                    "Which Basketball team has completed two threepeats?",
                    "Which Former NBA Player Was Nicknamed “Agent Zero” ?",
                    "What sport is dubbed the “king of sports”?",
                    "What is the name of the professional ice hockey team based in Toronto, Canada?",
                    "Who was the first gymnast to score a perfect 10 score?",
                    "Dump, floater, and wipe are terms used in which team sport?",
                    "Who was the first female driver to score points in a Grand Prix?"
            };
    public static String choices[][] =
            {
                    {"Greece and Australia", "USA and Germany", "Netherlands and Sweden", "Netherlands and Sweden"},
                    {"Philadelphia Eagles", "Miami Dolphins", "Green Bay Packers", "Baltimore Ravens"},
                    {"they both haven't won any", "they won the same amount", "Venus Williams", "Serena Williams"},
                    {"Los Angeles Lakers", "Chicago Bulls", "New York Knicks", "Cleveland Cavaliers"},
                    {"Kareem Abdul-Jabbar", "Rick Barry", "Gilbert Arenas", "Charles Barkley"},
                    {"Tennis", "Rugby", "Basketball", "Soccer"},
                    {"Toronto Marlies", "Toronto Maple Leafs", "North York Rangers", "Toronto Lions"},
                    {"Simone Biles", "Nastia Liukin", "Shannon Miller", "Nadia Comaneci"},
                    {"Volleyball", "Football", "Field hockey", "Figure skating"},
                    {"Maria Teresa de Filippis", "Lella Lombardi", "Divina Galica", "Desiré Wilson"}

            };
    public static String correctAnswers[] =
            {
                    "Greece and Australia",
                    "Miami Dolphins",
                    "Serena Williams",
                    "Chicago Bulls",
                    "Gilbert Arenas",
                    "Soccer",
                    "Toronto Maple Leafs",
                    "Nadia Comaneci",
                    "Volleyball",
                    "Lella Lombardi"
            };
}
